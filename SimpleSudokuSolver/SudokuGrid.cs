﻿using System;
using System.Linq;
using System.Collections.Generic;
namespace SimpleSudokuSolver
{
    class SudokuGrid
    {
        private List<Cell> _cells;
        public SudokuGrid(int?[] grid)
        {
            if (grid.Count() != 9 * 9)
                throw new ArgumentException();

            _cells = new List<Cell>(9 * 9);

            for (int y = 0; y < 9; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    int? value = grid[y * 9 + x];
                    Cell cell = new Cell(this, value, x, y);
                    _cells.Add(cell);
                }
            }
        }

        public SudokuGrid(SudokuGrid grid)
        {
            if (grid == null)
                throw new ArgumentNullException();

            _cells = new List<Cell>(9 * 9);

            for (int x = 0; x < 9; x++)
            {
                for (int y = 0; y < 9; y++)
                {
                    Cell cell = new Cell(this, grid.Get(x, y).Value, x, y);
                    _cells.Add(cell);
                }
            }
            
        }

        public List<Cell> ColumnOf(Cell cell)
        {
            List<Cell> column = _cells.Where(cell2 => cell2.X == cell.X).ToList();
            return column;
        }

        public List<Cell> RowOf(Cell cell)
        {
            List<Cell> row = _cells.Where(cell2 => cell2.Y == cell.Y).ToList();
            return row;
        }

        public List<Cell> BlockOf(Cell cell)
        {
            int blockX = cell.X - (cell.X % 3);
            int blockY = cell.Y - (cell.Y % 3);
            List<Cell> block =new List<Cell>();

            for (int x = blockX; x < blockX + 3; x++)
            {
                for (int y = blockY; y < blockY + 3; y++)
                {
                    block.Add(Get(x, y));
                }
            }

            return block;
        }

        public List<int> PossibleValues(Cell cell)
        {
            if(!cell.IsEmpty)
                return new List<int>(new int[]{(int)cell.Value});

            List<Cell> column = ColumnOf(cell).Where(cell2 => !cell2.IsEmpty).ToList();
            List<Cell> row = RowOf(cell).Where(cell2 => !cell2.IsEmpty).ToList();
            List<Cell> block = BlockOf(cell).Where(cell2 => !cell2.IsEmpty).ToList();


            List<int> fValues = new List<int>();
            fValues.AddRange(column.ConvertAll(cell2 => (int)cell2.Value).ToList());
            fValues.AddRange(row.ConvertAll(cell2 => (int)cell2.Value).ToList());
            fValues.AddRange(block.ConvertAll(cell2 => (int)cell2.Value).ToList());
            
            List<int> pValues = new List<int>();
            for (int i = 1; i <= 9; i++)
            {
                if (fValues.Contains(i) == false)
                    pValues.Add(i);
            }
            
            return pValues;
        }

        public Cell Get(int x, int y)
        {
            return _cells.Where(cell => cell.X == x && cell.Y == y).First<Cell>();
        }

        public bool? CantAccept(Cell cell, int value)
        {
            if(!PossibleValues(cell).Contains(value))
                return true;

            return null;
        }

        public List<Cell> EmptyCells()
        {
            return _cells.Where(cell => cell.IsEmpty).ToList();
        }
    }

    class Cell
    {
        private SudokuGrid _grid;
        private int _x, _y;
        private int? _value;
        public Cell(SudokuGrid grid, int x, int y)
        {
            if (x < 0 || x > 8 || y < 0 || y > 8)
                throw new IndexOutOfRangeException();

            _grid = grid;
            _x = x;
            _y = y;
            _value = null;
        }

        public Cell(SudokuGrid grid, int? value, int x, int y)
            : this(grid, x, y)
        {
            _value = value;
        }

        public int X
        {
            get { return _x; }
        }

        public int Y
        {
            get { return _y; }
        }

        public int? Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public bool IsEmpty
        {
            get { return _value == null ? true : false; }
        }
    }
}
