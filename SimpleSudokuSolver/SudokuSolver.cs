﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace SimpleSudokuSolver
{
    class SudokuSolver
    {
        private SudokuGrid _grid;
        public SudokuSolver(SudokuGrid grid)
        {
            if (grid == null)
                throw new ArgumentNullException();

            _grid = new SudokuGrid(grid);
        }

        public bool solve()
        {
            bool gridChanged;

            do
            {
                List<Cell> emptyCells = _grid.EmptyCells();

                gridChanged = false;

                foreach (Cell cell in emptyCells)
                {
                    int? value = investigateCell(cell);

                    if (value != null)
                    {
                        
                        gridChanged = true;
                        cell.Value = value;
                    }
                }

            } while (gridChanged);

            if (_grid.EmptyCells().Count() > 0)
                return false;

            return true;
        }

        int? investigateCell(Cell cell)
        {
            if (!cell.IsEmpty)
                return cell.Value;

            List<int> pValues = _grid.PossibleValues(cell);


            if (pValues.Count == 1)
                return (int?)pValues.First();

            List<Cell> block = _grid.BlockOf(cell);
            block.Remove(cell);

            foreach (int value in pValues)
            {
                int otherPossibleCellsCount = block.Where(cell2 => _grid.PossibleValues(cell2).Contains(value)).Count();
                if (otherPossibleCellsCount == 0)
                    return value;
            }

            return null;
        }

        public void printGrid()
        {
            Console.WriteLine("===================");
            for (int y = 0; y < 9; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    Console.Write("|");
                    if(_grid.Get(x,y).IsEmpty)
                        Console.Write("X");
                    else
                        Console.Write(_grid.Get(x, y).Value);
                }
                Console.WriteLine("|");
                Console.WriteLine("===================");
            }
        }
    }
}
